#[allow(unused_imports)]
extern crate glfw;

use glfw::{Context, Key, Action};

extern crate gl;

use std::sync::mpsc::Receiver;

mod hello_window;
use hello_window::hello_window::run;
// mod hello_triangle;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT:u32 = 600;

fn main() {
    run();
    // hello_triangle::hello_triangle::run();
}
