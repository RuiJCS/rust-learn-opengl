#[allow(unused_imports)]
#[allow(non_snake_case)]

#[macro_use]
extern crate c_string;

extern crate libc;
extern crate glfw;

mod camera;
use camera::Camera;

mod shader;
use shader::ShaderProgram;

use glfw::{Context, Key, Action};

extern crate gl;

use self::gl::types::*;
use std::sync::mpsc::Receiver;
use std::ptr;
use std::mem;
use std::os::raw::c_void;
use std::path::Path;

extern crate cgmath;
use cgmath::{Matrix4, vec3,  Rad, Vector3};
use cgmath::prelude::*;

extern crate image;
use image::GenericImage;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT:u32 = 600;

unsafe fn gen_ebo() -> u32 {

	let vertices: [f32; 180] = [
    -0.5, -0.5, -0.5,  0.0, 0.0,
     0.5, -0.5, -0.5,  1.0, 0.0,
     0.5,  0.5, -0.5,  1.0, 1.0,
     0.5,  0.5, -0.5,  1.0, 1.0,
    -0.5,  0.5, -0.5,  0.0, 1.0,
    -0.5, -0.5, -0.5,  0.0, 0.0,

    -0.5, -0.5,  0.5,  0.0, 0.0,
     0.5, -0.5,  0.5,  1.0, 0.0,
     0.5,  0.5,  0.5,  1.0, 1.0,
     0.5,  0.5,  0.5,  1.0, 1.0,
    -0.5,  0.5,  0.5,  0.0, 1.0,
    -0.5, -0.5,  0.5,  0.0, 0.0,

    -0.5,  0.5,  0.5,  0.0, 0.0,
    -0.5,  0.5, -0.5,  1.0, 0.0,
    -0.5, -0.5, -0.5,  1.0, 1.0,
    -0.5, -0.5, -0.5,  1.0, 1.0,
    -0.5, -0.5,  0.5,  0.0, 1.0,
    -0.5,  0.5,  0.5,  0.0, 0.0,

      0.5,  0.5,  0.5,  0.0, 0.0,
     0.5,  0.5, -0.5,  1.0, 0.0,
     0.5, -0.5, -0.5,  1.0, 1.0,
     0.5, -0.5, -0.5,  1.0, 1.0,
     0.5, -0.5,  0.5,  0.0, 1.0,
     0.5,  0.5,  0.5,  0.0, 0.0,

    -0.5, -0.5, -0.5,  0.0, 0.0,
     0.5, -0.5, -0.5,  1.0, 0.0,
     0.5, -0.5,  0.5,  1.0, 1.0,
     0.5, -0.5,  0.5,  1.0, 1.0,
    -0.5, -0.5,  0.5,  0.0, 1.0,
    -0.5, -0.5, -0.5,  0.0, 0.0,

    -0.5,  0.5, -0.5,  0.0, 0.0,
     0.5,  0.5, -0.5,  1.0, 0.0,
     0.5,  0.5,  0.5,  1.0, 1.0,
     0.5,  0.5,  0.5,  1.0, 1.0,
    -0.5,  0.5,  0.5,  0.0, 1.0,
    -0.5,  0.5, -0.5,  0.0, 0.0
	];

    let indices = [
        0, 1, 2,   // first triangle
        2, 3, 0,   // second triangle

    ];

    let mut vao = 0;
    gl::GenVertexArrays(1,&mut vao);
    gl::BindVertexArray(vao);
    let mut vbo = 0;
    gl::GenBuffers(1, &mut vbo);
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    let mut ebo = 0;
    gl::GenBuffers(1, &mut ebo);
    gl::BufferData( gl::ARRAY_BUFFER,
                    (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                    &vertices[0] as *const f32 as *const c_void,
                    gl::STATIC_DRAW);
    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
    gl::BufferData(gl::ELEMENT_ARRAY_BUFFER,
                    (indices.len() * mem::size_of::<GLint>()) as GLsizeiptr,
                    &indices[0] as *const i32 as *const c_void,
                    gl::STATIC_DRAW);

    let stride = 5 * mem::size_of::<GLfloat>() as GLsizei;
    // position attribute
    gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, stride, ptr::null());
    gl::EnableVertexAttribArray(0);
    // texture coord attribute
    gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE, stride, (3 * mem::size_of::<GLfloat>()) as *const c_void);
    gl::EnableVertexAttribArray(1);
    
    vao
}

unsafe fn gen_textures() -> (u32,u32) {
    let border_color: [f32;4] = [ 
        1.0,
        1.0,
        0.0,
        1.0
    ];
    // Brick Wall
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::MIRRORED_REPEAT as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::MIRRORED_REPEAT as i32);
    gl::TexParameterfv(gl::TEXTURE_2D, gl::TEXTURE_BORDER_COLOR, &border_color as *const f32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    let image1 = image::open(Path::new("resources/wall.jpg")).unwrap();
    let mut texture1 = 0;
    let data = image1.raw_pixels();
    gl::GenTextures(1, &mut texture1);
    gl::BindTexture(gl::TEXTURE_2D, texture1); 
    gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, image1.width() as i32, image1.height() as i32, 0, gl::RGB, gl::UNSIGNED_BYTE, &data[0] as *const u8 as *const c_void);
    gl::GenerateMipmap(gl::TEXTURE_2D);
    // Smiley Face
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::MIRRORED_REPEAT as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::MIRRORED_REPEAT as i32);
    gl::TexParameterfv(gl::TEXTURE_2D, gl::TEXTURE_BORDER_COLOR, &border_color as *const f32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    let image2 = image::open(Path::new("resources/awesomeface.png")).unwrap();
    let mut texture2 = 0;
    let data = image2.flipv().raw_pixels();
    gl::GenTextures(1, &mut texture2);
    gl::BindTexture(gl::TEXTURE_2D, texture2); 
    gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, image2.width() as i32, image2.height() as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, &data[0] as *const u8 as *const c_void);
    gl::GenerateMipmap(gl::TEXTURE_2D);
    
    (texture1,texture2)
}

fn degree_to_rads(d: f32) -> f32 {
	d * 3.14 / 180.0
}

pub fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
    glfw.window_hint(glfw::WindowHint::ContextVersion(3,3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(glfw::OpenGlProfileHint::Core));

    #[cfg(target_os = "macos")]
    glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

    let (mut window, events) = glfw.create_window(WINDOW_WIDTH, WINDOW_HEIGHT, "LearnOpenGL", glfw::WindowMode::Windowed)
        .expect("Failed to create GLFW window");
    
    window.make_current();
    window.set_key_polling(true);
    window.set_cursor_pos_polling(true);
    window.set_framebuffer_size_polling(true);
    window.set_cursor_mode(glfw::CursorMode::Disabled);
    

    gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
    let mut nr_attributes = 0;
    unsafe { 
        gl::Viewport(0, 0, WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32);
        gl::PolygonMode(gl::FRONT,gl::FILL);
        gl::GetIntegerv(gl::MAX_VERTEX_ATTRIBS, &mut nr_attributes);
		gl::Enable(gl::DEPTH_TEST);
    }
    
    println!("{}",nr_attributes);

    let vao = unsafe { gen_ebo() };
    
    let shader_program = ShaderProgram::gen_shaders();

    let textures = unsafe { gen_textures() };
    let texture1 = textures.0;
    let texture2 = textures.1;
    // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    // -------------------------------------------------------------------------------------------
    // gl::UseProgram(shader_program); // don't forget to activate/use the shader before setting uniforms!
    shader_program.use_shader();
    shader_program.set_int("texture1", 0);
    shader_program.set_int("texture2", 1);

    // FOV,Aspect ratio,near?,far?
	let projection: Matrix4<f32> = cgmath::perspective(Rad(degree_to_rads(45.0)), WINDOW_WIDTH as f32 / WINDOW_HEIGHT as f32, 0.1, 100.0);
	let model: Matrix4<f32> = Matrix4::identity();

	let cube_position: [Vector3<f32>; 10] = [
		vec3( 0.0,  0.0,  0.0), 
		vec3( 2.0,  5.0, -15.0), 
		vec3(-1.5, -2.2, -2.5),  
		vec3(-3.8, -2.0, -12.3),  
		vec3( 2.4, -0.4, -3.5),  
		vec3(-1.7,  3.0, -7.5),  
		vec3( 1.3, -2.0, -2.5),  
		vec3( 1.5,  2.0, -2.5), 
		vec3( 1.5,  0.2, -1.5), 
		vec3(-1.3,  1.0, -1.5)  
	];

    let (mouse_x, mouse_y) = window.get_cursor_pos();
    let mut cam = Camera::new(glfw.get_time() as f32, mouse_x,mouse_y);
    cam.turn_camera(mouse_x,mouse_y);

    // render loop
    // -----------
    while !window.should_close() {
        // events
        // -----
        let time = glfw.get_time() as f32;
        process_events(&mut window, &events, &mut cam, time);

        // let view: Matrix4<f32> = Matrix4::identity() * Matrix4::<f32>::from_translation(vec3(0.0,0.0,-3.0));
        let view = cam.look_at(time);

        unsafe {
            gl::ClearColor(0.2, 0.3, 0.3, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            shader_program.use_shader();
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, texture1);
            gl::ActiveTexture(gl::TEXTURE1);
            gl::BindTexture(gl::TEXTURE_2D, texture2);
            shader_program.set_matrices(model, view, projection);
            gl::BindVertexArray(vao);
            // gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, ptr::null());
			for (i,position) in cube_position.iter().enumerate() {
                let mut angle = 20.0 * i as f32;
				if i % 3 == 0 || i == 1 {
					angle = angle * time as f32 * 0.7 ;
				}
				let local_model = Matrix4::from_translation(*position) * Matrix4::<f32>::from_axis_angle(vec3(1.0, 0.3, 0.5).normalize(), Rad(degree_to_rads(angle)));
                shader_program.set_model_matrix(local_model);
				gl::DrawArrays(gl::TRIANGLES,0,36);
			}
			// gl::DrawArrays(gl::TRIANGLES,0,36);
        };

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        window.swap_buffers();
        glfw.poll_events();
    }

}

fn process_events(window: &mut glfw::Window, events: &Receiver<(f64, glfw::WindowEvent)>, cam: &mut Camera, time: f32) {
    for (_, event) in glfw::flush_messages(events) {
        match event {
            glfw::WindowEvent::FramebufferSize(width, height) => {
                // make sure the viewport matches the new window dimensions; note that width and
                // height will be significantly larger than specified on retina displays.
                unsafe { gl::Viewport(0, 0, width, height) }
            }
            glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => window.set_should_close(true),
            glfw::WindowEvent::Key(Key::F, _, Action::Press, _) => unsafe {gl::PolygonMode(gl::FRONT_AND_BACK,gl::FILL)},
            glfw::WindowEvent::Key(Key::L, _, Action::Press, _) => unsafe {gl::PolygonMode(gl::FRONT_AND_BACK,gl::LINE)},
            glfw::WindowEvent::Key(Key::W, _, Action::Repeat, _) => {cam.move_camera(1.0, 0.0, time)},
            glfw::WindowEvent::Key(Key::S, _, Action::Repeat, _) => {cam.move_camera(-1.0, 0.0, time)},
            glfw::WindowEvent::Key(Key::A, _, Action::Repeat, _) => {cam.move_camera(0.0, 1.0, time)},
            glfw::WindowEvent::Key(Key::D, _, Action::Repeat, _) => {cam.move_camera(0.0, -1.0, time)},
            glfw::WindowEvent::CursorPos(xpos, ypos) => {cam.turn_camera(xpos, ypos)},
            _ => {}
        }
    }
}