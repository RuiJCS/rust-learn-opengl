#[allow(non_snake_case)]
#[allow(unused_imports)]

pub mod hello_window  {
    extern crate glfw;

    use glfw::{Context, Key, Action};

    extern crate gl;
    use std::sync::mpsc::Receiver;

    const WINDOW_WIDTH: u32 = 800;
    const WINDOW_HEIGHT:u32 = 600;

    pub fn run() {
        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
        glfw.window_hint(glfw::WindowHint::ContextVersion(3,3));
        glfw.window_hint(glfw::WindowHint::OpenGlProfile(glfw::OpenGlProfileHint::Core));

        #[cfg(target_os = "macos")]
        glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

        let (mut window, events) = glfw.create_window(WINDOW_WIDTH, WINDOW_HEIGHT, "LearnOpenGL", glfw::WindowMode::Windowed)
            .expect("Failed to create GLFW window");
        
        window.make_current();
        window.set_key_polling(true);
        // window.set_framebuffer_size_polling(true);

        gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
        unsafe { gl::Viewport(0, 0, WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32); }

        // render loop
        // -----------
        while !window.should_close() {
            // events
            // -----
            process_events(&mut window, &events);

            unsafe { gl::ClearColor(0.2, 0.3, 0.3, 1.0); }
            unsafe { gl::Clear(gl::COLOR_BUFFER_BIT); }

            // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
            // -------------------------------------------------------------------------------
            window.swap_buffers();
            glfw.poll_events();
        }

    }

    fn process_events(window: &mut glfw::Window, events: &Receiver<(f64, glfw::WindowEvent)>) {
        for (_, event) in glfw::flush_messages(events) {
            match event {
                glfw::WindowEvent::FramebufferSize(width, height) => {
                    // make sure the viewport matches the new window dimensions; note that width and
                    // height will be significantly larger than specified on retina displays.
                    unsafe { gl::Viewport(0, 0, width, height) }
                }
                glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => window.set_should_close(true),
                _ => {}
            }
        }
    }

}

