#[allow(unused_imports)]
#[allow(non_snake_case)]
#[macro_use]
extern crate c_string;

extern crate glfw;
extern crate libc;

use glfw::{Action, Context, Key};

extern crate gl;

use self::gl::types::*;
use std::ffi::CString;
use std::fs::File;
use std::io::prelude::*;
use std::mem;
use std::os::raw::c_void;
use std::path::Path;
use std::ptr;
use std::str;
use std::sync::mpsc::Receiver;

extern crate cgmath;
use cgmath::prelude::*;
use cgmath::{vec3, Matrix4, Rad};

extern crate image;
use image::GenericImage;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT: u32 = 600;

unsafe fn gen_shaders() -> u32 {
    let mut f = File::open("shaders/transform.vert").expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");
    let copy = contents.clone();
    let vertex_shader_source = copy.as_str();
    f = File::open("shaders/transform.frag").expect("file not found");
    contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");
    let fragment_shader_source = &contents.as_str();
    //creating the vertex shader
    let vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
    let vertex_shader_c = CString::new(vertex_shader_source.as_bytes()).unwrap();
    gl::ShaderSource(vertex_shader, 1, &vertex_shader_c.as_ptr(), ptr::null());
    gl::CompileShader(vertex_shader);

    //checking for errors in the vertex shader
    let mut result = gl::FALSE as GLint;
    let mut msg = Vec::with_capacity(512);
    msg.set_len(512 - 1);
    gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut result);

    if result != gl::TRUE as GLint {
        gl::GetShaderInfoLog(
            vertex_shader,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "VERTEX ERROR: The shader couldn't compile: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    //creating the fragment shader
    let fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
    let fragment_shader_c = CString::new(fragment_shader_source.as_bytes()).unwrap();
    gl::ShaderSource(fragment_shader, 1, &fragment_shader_c.as_ptr(), ptr::null());
    gl::CompileShader(fragment_shader);

    // Checking for errors in the fragment shader
    gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut result);
    if result != gl::TRUE as GLint {
        gl::GetShaderInfoLog(
            fragment_shader,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "FRAGMENT ERROR: The shader couldn't compile: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    // Creating the shader program (linking the shaders)
    let shader_program = gl::CreateProgram();
    gl::AttachShader(shader_program, vertex_shader);
    gl::AttachShader(shader_program, fragment_shader);
    gl::LinkProgram(shader_program);

    // Checking for errors in the shader program
    gl::GetProgramiv(shader_program, gl::LINK_STATUS, &mut result);

    if result != gl::TRUE as GLint {
        gl::GetProgramInfoLog(
            shader_program,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "LINKING ERROR: The program couldn't be linked: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    gl::DeleteShader(vertex_shader);
    gl::DeleteShader(fragment_shader);

    shader_program
}

unsafe fn gen_ebo() -> u32 {
    let vertices: [f32; 32] = [
        // positions       // colors        // texture coords
        0.5, 0.5, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, // top right
        0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, // bottom right
        -0.5, -0.5, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, // bottom left
        -0.5, 0.5, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, // top left
    ];

    let indices = [
        0, 1, 2, // first triangle
        2, 3, 0, // second triangle
    ];
    let mut vao = 0;
    gl::GenVertexArrays(1, &mut vao);
    gl::BindVertexArray(vao);
    let mut vbo = 0;
    gl::GenBuffers(1, &mut vbo);
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    let mut ebo = 0;
    gl::GenBuffers(1, &mut ebo);
    gl::BufferData(
        gl::ARRAY_BUFFER,
        (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
        &vertices[0] as *const f32 as *const c_void,
        gl::STATIC_DRAW,
    );
    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
    gl::BufferData(
        gl::ELEMENT_ARRAY_BUFFER,
        (indices.len() * mem::size_of::<GLint>()) as GLsizeiptr,
        &indices[0] as *const i32 as *const c_void,
        gl::STATIC_DRAW,
    );

    let stride = 8 * mem::size_of::<GLfloat>() as GLsizei;
    // position attribute
    gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, stride, ptr::null());
    gl::EnableVertexAttribArray(0);
    // texture coord attribute
    gl::VertexAttribPointer(
        1,
        2,
        gl::FLOAT,
        gl::FALSE,
        stride,
        (6 * mem::size_of::<GLfloat>()) as *const c_void,
    );
    gl::EnableVertexAttribArray(1);

    vao
}

unsafe fn gen_textures() -> (u32, u32) {
    let border_color: [f32; 4] = [1.0, 1.0, 0.0, 1.0];
    // Brick Wall
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_WRAP_S,
        gl::MIRRORED_REPEAT as i32,
    );
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_WRAP_T,
        gl::MIRRORED_REPEAT as i32,
    );
    gl::TexParameterfv(
        gl::TEXTURE_2D,
        gl::TEXTURE_BORDER_COLOR,
        &border_color as *const f32,
    );
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_MIN_FILTER,
        gl::LINEAR_MIPMAP_LINEAR as i32,
    );
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    let image1 = image::open(Path::new("resources/wall.jpg")).unwrap();
    let mut texture1 = 0;
    let data = image1.raw_pixels();
    gl::GenTextures(1, &mut texture1);
    gl::BindTexture(gl::TEXTURE_2D, texture1);
    gl::TexImage2D(
        gl::TEXTURE_2D,
        0,
        gl::RGB as i32,
        image1.width() as i32,
        image1.height() as i32,
        0,
        gl::RGB,
        gl::UNSIGNED_BYTE,
        &data[0] as *const u8 as *const c_void,
    );
    gl::GenerateMipmap(gl::TEXTURE_2D);
    // Smiley Face
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_WRAP_S,
        gl::MIRRORED_REPEAT as i32,
    );
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_WRAP_T,
        gl::MIRRORED_REPEAT as i32,
    );
    gl::TexParameterfv(
        gl::TEXTURE_2D,
        gl::TEXTURE_BORDER_COLOR,
        &border_color as *const f32,
    );
    gl::TexParameteri(
        gl::TEXTURE_2D,
        gl::TEXTURE_MIN_FILTER,
        gl::LINEAR_MIPMAP_LINEAR as i32,
    );
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    let image2 = image::open(Path::new("resources/awesomeface.png")).unwrap();
    let mut texture2 = 0;
    let data = image2.flipv().raw_pixels();
    gl::GenTextures(1, &mut texture2);
    gl::BindTexture(gl::TEXTURE_2D, texture2);
    gl::TexImage2D(
        gl::TEXTURE_2D,
        0,
        gl::RGB as i32,
        image2.width() as i32,
        image2.height() as i32,
        0,
        gl::RGBA,
        gl::UNSIGNED_BYTE,
        &data[0] as *const u8 as *const c_void,
    );
    gl::GenerateMipmap(gl::TEXTURE_2D);

    (texture1, texture2)
}

unsafe fn gen_matrices(time: f32) -> Matrix4<f32> {
    let mut trans: Matrix4<f32> = Matrix4::identity();
    trans = trans * Matrix4::<f32>::from_scale(0.5);
    trans = trans
        * Matrix4::<f32>::from_translation(vec3(Rad::cos(Rad(time)), Rad::sin(Rad(time)), 0.0));
    trans = trans * Matrix4::<f32>::from_angle_z(Rad(time * 10.0));
    trans
}

pub fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
    glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));

    #[cfg(target_os = "macos")]
    glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

    let (mut window, events) = glfw
        .create_window(
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            "LearnOpenGL",
            glfw::WindowMode::Windowed,
        )
        .expect("Failed to create GLFW window");

    window.make_current();
    window.set_key_polling(true);
    window.set_framebuffer_size_polling(true);

    gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
    let mut nr_attributes = 0;
    unsafe {
        gl::Viewport(0, 0, WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32);
        gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
        gl::GetIntegerv(gl::MAX_VERTEX_ATTRIBS, &mut nr_attributes);
    }

    println!("{}", nr_attributes);

    let vao = unsafe { gen_ebo() };

    let shader_program = unsafe { gen_shaders() };

    let textures = unsafe { gen_textures() };
    let texture1 = textures.0;
    let texture2 = textures.1;
    unsafe {
        // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
        // -------------------------------------------------------------------------------------------
        gl::UseProgram(shader_program); // don't forget to activate/use the shader before setting uniforms!
        gl::Uniform1i(
            gl::GetUniformLocation(shader_program, c_str!("texture1").as_ptr()),
            0,
        );
        gl::Uniform1i(
            gl::GetUniformLocation(shader_program, c_str!("texture2").as_ptr()),
            1,
        );
    };

    // render loop
    // -----------
    while !window.should_close() {
        // events
        // -----
        process_events(&mut window, &events);
        unsafe {
            gl::ClearColor(0.2, 0.3, 0.3, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::UseProgram(shader_program);
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, texture1);
            gl::ActiveTexture(gl::TEXTURE1);
            gl::BindTexture(gl::TEXTURE_2D, texture2);
            let transform = gen_matrices(glfw.get_time() as f32);
            let uni_loc = gl::GetUniformLocation(shader_program, c_str!("transform").as_ptr());
            gl::UniformMatrix4fv(uni_loc, 1, gl::FALSE, transform.as_ptr());
            gl::BindVertexArray(vao);
            gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, ptr::null());
        };

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        window.swap_buffers();
        glfw.poll_events();
    }
}

fn process_events(window: &mut glfw::Window, events: &Receiver<(f64, glfw::WindowEvent)>) {
    for (_, event) in glfw::flush_messages(events) {
        match event {
            glfw::WindowEvent::FramebufferSize(width, height) => {
                // make sure the viewport matches the new window dimensions; note that width and
                // height will be significantly larger than specified on retina displays.
                unsafe { gl::Viewport(0, 0, width, height) }
            }
            glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                window.set_should_close(true)
            }
            glfw::WindowEvent::Key(Key::F, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL)
            },
            glfw::WindowEvent::Key(Key::L, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE)
            },
            _ => {}
        }
    }
}
