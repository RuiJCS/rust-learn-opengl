#[allow(unused_imports)]
#[allow(non_snake_case)]
extern crate glfw;

use glfw::{Action, Context, Key};

extern crate gl;

use self::gl::types::*;
use std::ffi::CString;
use std::mem;
use std::os::raw::c_void;
use std::ptr;
use std::str;
use std::sync::mpsc::Receiver;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT: u32 = 600;

const VERTEX_SHADER_SOURCE: &str = r#"
    #version 330 core
    layout (location = 0) in vec3 aPos;
    void main() {
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
    }
"#;

const FRAGMENT_SHADER_SOURCE: &str = r#"
    #version 330 core
    out vec4 FragColor;
    void main() {
    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
    }
"#;

unsafe fn gen_vbo() -> u32 {
    let vertices: [f32; 18] = [
        0.5, 0.5, 0.0, // top right
        0.5, -0.5, 0.0, // bottom right
        -0.5, -0.5, 0.0, // bottom left
        0.5, 0.5, 0.0, // top right
        -0.5, 0.5, 0.0, // top left
        -0.5, -0.5, 0.0, // bottom left
    ];
    let mut vao = 0;
    gl::GenVertexArrays(1, &mut vao);
    gl::BindVertexArray(vao);
    let mut vbo = 0;
    gl::GenBuffers(1, &mut vbo);
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::BufferData(
        gl::ARRAY_BUFFER,
        (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
        &vertices[0] as *const f32 as *const c_void,
        gl::STATIC_DRAW,
    );

    gl::VertexAttribPointer(
        0,
        3,
        gl::FLOAT,
        gl::FALSE,
        3 * mem::size_of::<GLfloat>() as GLsizei,
        ptr::null(),
    );
    gl::EnableVertexAttribArray(0);
    vao
}

unsafe fn gen_shaders() -> u32 {
    //creating the vertex shader
    let vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
    let vertex_shader_c = CString::new(VERTEX_SHADER_SOURCE.as_bytes()).unwrap();
    gl::ShaderSource(vertex_shader, 1, &vertex_shader_c.as_ptr(), ptr::null());
    gl::CompileShader(vertex_shader);

    //checking for errors in the vertex shader
    let mut result = gl::FALSE as GLint;
    let mut msg = Vec::with_capacity(512);
    msg.set_len(512 - 1);
    gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut result);

    if result != gl::TRUE as GLint {
        gl::GetShaderInfoLog(
            vertex_shader,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "VERTEX ERROR: The shader couldn't compile: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    //creating the fragment shader
    let fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
    let fragment_shader_c = CString::new(FRAGMENT_SHADER_SOURCE.as_bytes()).unwrap();
    gl::ShaderSource(fragment_shader, 1, &fragment_shader_c.as_ptr(), ptr::null());
    gl::CompileShader(fragment_shader);

    // Checking for errors in the fragment shader
    gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut result);
    if result != gl::TRUE as GLint {
        gl::GetShaderInfoLog(
            fragment_shader,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "FRAGMENT ERROR: The shader couldn't compile: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    // Creating the shader program (linking the shaders)
    let shader_program = gl::CreateProgram();
    gl::AttachShader(shader_program, vertex_shader);
    gl::AttachShader(shader_program, fragment_shader);
    gl::LinkProgram(shader_program);

    // Checking for errors in the shader program
    gl::GetProgramiv(shader_program, gl::LINK_STATUS, &mut result);

    if result != gl::TRUE as GLint {
        gl::GetProgramInfoLog(
            shader_program,
            512,
            ptr::null_mut(),
            msg.as_mut_ptr() as *mut GLchar,
        );
        panic!(
            "LINKING ERROR: The program couldn't be linked: {}",
            str::from_utf8(&msg).unwrap()
        );
    }

    gl::DeleteShader(vertex_shader);
    gl::DeleteShader(fragment_shader);

    shader_program
}

pub fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
    glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));

    #[cfg(target_os = "macos")]
    glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

    let (mut window, events) = glfw
        .create_window(
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            "LearnOpenGL",
            glfw::WindowMode::Windowed,
        )
        .expect("Failed to create GLFW window");

    window.make_current();
    window.set_key_polling(true);
    window.set_framebuffer_size_polling(true);

    gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);

    unsafe {
        gl::Viewport(0, 0, WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32);
        gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
    }

    let vao = unsafe { gen_vbo() };
    // let vao = unsafe { gen_ebo() };

    let shader_program = unsafe { gen_shaders() };

    // render loop
    // -----------
    while !window.should_close() {
        // events
        // -----
        process_events(&mut window, &events);
        unsafe {
            gl::ClearColor(0.2, 0.3, 0.3, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::UseProgram(shader_program);
            gl::BindVertexArray(vao);
            // gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, ptr::null());
            // uncomment this line and comment the line above to use draw arrays
            gl::DrawArrays(gl::TRIANGLES, 0, 6);
        };

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        window.swap_buffers();
        glfw.poll_events();
    }
}

fn process_events(window: &mut glfw::Window, events: &Receiver<(f64, glfw::WindowEvent)>) {
    for (_, event) in glfw::flush_messages(events) {
        match event {
            glfw::WindowEvent::FramebufferSize(width, height) => {
                // make sure the viewport matches the new window dimensions; note that width and
                // height will be significantly larger than specified on retina displays.
                unsafe { gl::Viewport(0, 0, width, height) }
            }
            glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                window.set_should_close(true)
            }
            glfw::WindowEvent::Key(Key::F, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL)
            },
            glfw::WindowEvent::Key(Key::L, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE)
            },
            _ => {}
        }
    }
}
