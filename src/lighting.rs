#[allow(unused_imports)]
#[allow(non_snake_case)]
#[macro_use]
extern crate c_string;

extern crate glfw;
extern crate libc;

mod utils;
use utils::{camera, common, shader};

use camera::Camera;

use shader::ShaderProgram;

use common::{degree_to_rads, gen_ebo, gen_ebo_with_normals};

use glfw::{Action, Context, Key};

extern crate gl;
use std::sync::mpsc::Receiver;

extern crate cgmath;
use cgmath::prelude::*;
use cgmath::{vec3, Matrix4, Rad, Vector3};

extern crate image;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT: u32 = 600;

pub fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
    glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));

    #[cfg(target_os = "macos")]
    glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

    let (mut window, events) = glfw
        .create_window(
            WINDOW_WIDTH,
            WINDOW_HEIGHT,
            "LearnOpenGL",
            glfw::WindowMode::Windowed,
        )
        .expect("Failed to create GLFW window");

    window.make_current();
    window.set_key_polling(true);
    window.set_cursor_pos_polling(true);
    window.set_framebuffer_size_polling(true);
    window.set_cursor_mode(glfw::CursorMode::Disabled);

    gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
    let mut nr_attributes = 0;
    unsafe {
        gl::Viewport(0, 0, WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32);
        gl::PolygonMode(gl::FRONT, gl::FILL);
        gl::GetIntegerv(gl::MAX_VERTEX_ATTRIBS, &mut nr_attributes);
        gl::Enable(gl::DEPTH_TEST);
    }

    println!("{}", nr_attributes);
    // FOV,Aspect ratio,near?,far?
    let projection: Matrix4<f32> = cgmath::perspective(
        Rad(degree_to_rads(45.0)),
        WINDOW_WIDTH as f32 / WINDOW_HEIGHT as f32,
        0.1,
        100.0,
    );
    let cube_position: [Vector3<f32>; 1] = [vec3(1.2, 3.0, 2.0)];
    let model: Matrix4<f32> = Matrix4::identity() + Matrix4::from_translation(cube_position[0]);
    let model = model * Matrix4::from_scale(0.2);

    let (mouse_x, mouse_y) = window.get_cursor_pos();
    let mut cam = Camera::new_with_position(
        vec3(0.0, 0.5, 3.0),
        glfw.get_time() as f32,
        mouse_x,
        mouse_y,
    );
    cam.turn_camera(mouse_x, mouse_y);
    let light_vao = 1;
    unsafe { gen_ebo_with_normals(light_vao) };
    let cube_vao = 2;
    unsafe { gen_ebo_with_normals(cube_vao) };

    let lighting_shader = ShaderProgram::gen_shaders_names(
        "shaders/phong.vs".to_string(),
        "shaders/phong.fs".to_string(),
    );
    lighting_shader.use_shader();
    lighting_shader.set_vec3("objectColor", 1.0, 0.5, 0.31);
    lighting_shader.set_vec3("lightColor", 1.0, 1.0, 1.0);
    lighting_shader.set_float("ambientStrength", 0.1);
    lighting_shader.set_float("specularStrength", 0.5);
    lighting_shader.set_vec3("lightPos", 1.2, 3.0, 2.0);
    lighting_shader.set_vec3("viewPos", cam.position.x, cam.position.y, cam.position.z);

    let light_cube_shader = ShaderProgram::gen_shaders_names(
        "shaders/colors.vs".to_string(),
        "shaders/white.fs".to_string(),
    );

    // render loop
    // -----------
    while !window.should_close() {
        // events
        // -----
        let time = glfw.get_time() as f32;
        process_events(&mut window, &events, &mut cam, time);

        // let view: Matrix4<f32> = Matrix4::identity() * Matrix4::<f32>::from_translation(vec3(0.0,0.0,-3.0));
        let view = cam.look_at(time);

        unsafe {
            gl::ClearColor(0.2, 0.3, 0.3, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            lighting_shader.use_shader();
            lighting_shader.set_matrices(Matrix4::identity(), view, projection);
            lighting_shader.set_vec3("viewPos", cam.position.x, cam.position.y, cam.position.z);
            gl::BindVertexArray(cube_vao);
            gl::DrawArrays(gl::TRIANGLES, 0, 36);
            light_cube_shader.use_shader();
            light_cube_shader.set_matrices(model, view, projection);
            gl::BindVertexArray(light_vao);
            gl::DrawArrays(gl::TRIANGLES, 0, 36);
        };

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        window.swap_buffers();
        glfw.poll_events();
    }
}

fn process_events(
    window: &mut glfw::Window,
    events: &Receiver<(f64, glfw::WindowEvent)>,
    cam: &mut Camera,
    time: f32,
) {
    for (_, event) in glfw::flush_messages(events) {
        match event {
            glfw::WindowEvent::FramebufferSize(width, height) => {
                // make sure the viewport matches the new window dimensions; note that width and
                // height will be significantly larger than specified on retina displays.
                unsafe { gl::Viewport(0, 0, width, height) }
            }
            glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                window.set_should_close(true)
            }
            glfw::WindowEvent::Key(Key::F, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL)
            },
            glfw::WindowEvent::Key(Key::L, _, Action::Press, _) => unsafe {
                gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE)
            },
            glfw::WindowEvent::Key(Key::W, _, Action::Press, _) => cam.move_camera(1.0, 0.0, time),
            glfw::WindowEvent::Key(Key::S, _, Action::Press, _) => cam.move_camera(-1.0, 0.0, time),
            glfw::WindowEvent::Key(Key::U, _, Action::Press, _) => cam.upwards(),
            glfw::WindowEvent::Key(Key::I, _, Action::Press, _) => cam.downwards(),
            glfw::WindowEvent::Key(Key::A, _, Action::Press, _) => cam.move_camera(0.0, 1.0, time),
            glfw::WindowEvent::Key(Key::D, _, Action::Press, _) => cam.move_camera(0.0, -1.0, time),
            glfw::WindowEvent::CursorPos(xpos, ypos) => cam.turn_camera(xpos, ypos),
            _ => {}
        }
    }
}
