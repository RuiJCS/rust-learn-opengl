#![allow(dead_code)]
use std::fs::File;

use cgmath::prelude::*;
use cgmath::{vec3, Matrix4, Vector3};
use gl::types::*;
use std::ffi::CString;
use std::io::prelude::*;
use std::ptr;
use std::str;

pub struct ShaderProgram {
    shader_program: u32,
}

impl ShaderProgram {
    pub fn gen_shaders() -> Self {
        ShaderProgram::gen_shaders_names(
            "shaders/coordinates.vert".to_string(),
            "shaders/coordinates.frag".to_string(),
        )
    }

    pub fn gen_shaders_names(vert_name: String, frag_name: String) -> Self {
        let mut f = File::open(vert_name).expect("file not found");
        let mut contents = String::new();
        f.read_to_string(&mut contents)
            .expect("something went wrong reading the file");
        let copy = contents.clone();
        let vertex_shader_source = copy.as_str();
        f = File::open(&frag_name).expect(format!("{}  file not found", &frag_name).as_str());
        contents = String::new();
        f.read_to_string(&mut contents)
            .expect("something went wrong reading the file");
        let fragment_shader_source = &contents.as_str();
        unsafe {
            //creating the vertex shader
            let vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
            let vertex_shader_c = CString::new(vertex_shader_source.as_bytes()).unwrap();
            gl::ShaderSource(vertex_shader, 1, &vertex_shader_c.as_ptr(), ptr::null());
            gl::CompileShader(vertex_shader);

            //checking for errors in the vertex shader
            let mut result = gl::FALSE as GLint;
            let mut msg = Vec::with_capacity(512);
            msg.set_len(512);
            gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut result);

            if result != gl::TRUE as GLint {
                gl::GetShaderInfoLog(
                    vertex_shader,
                    512,
                    ptr::null_mut(),
                    msg.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "VERTEX ERROR: The shader couldn't compile: {}",
                    str::from_utf8(&msg).unwrap()
                );
            }

            //creating the fragment shader
            let fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
            let fragment_shader_c = CString::new(fragment_shader_source.as_bytes()).unwrap();
            gl::ShaderSource(fragment_shader, 1, &fragment_shader_c.as_ptr(), ptr::null());
            gl::CompileShader(fragment_shader);

            // Checking for errors in the fragment shader
            gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut result);
            if result != gl::TRUE as GLint {
                gl::GetShaderInfoLog(
                    fragment_shader,
                    512,
                    ptr::null_mut(),
                    msg.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "FRAGMENT ERROR: The shader couldn't compile: {}",
                    str::from_utf8(&msg).unwrap()
                );
            }

            // Creating the shader program (linking the shaders)
            let shader_program = gl::CreateProgram();
            gl::AttachShader(shader_program, vertex_shader);
            gl::AttachShader(shader_program, fragment_shader);
            gl::LinkProgram(shader_program);

            // Checking for errors in the shader program
            gl::GetProgramiv(shader_program, gl::LINK_STATUS, &mut result);

            if result != gl::TRUE as GLint {
                gl::GetProgramInfoLog(
                    shader_program,
                    512,
                    ptr::null_mut(),
                    msg.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "LINKING ERROR: The program couldn't be linked: {}",
                    str::from_utf8(&msg).unwrap()
                );
            }

            gl::DeleteShader(vertex_shader);
            gl::DeleteShader(fragment_shader);

            ShaderProgram { shader_program }
        }
    }

    pub fn use_shader(&self) {
        unsafe {
            gl::UseProgram(self.shader_program);
        }
    }

    pub fn set_int(&self, name: &str, value: i32) {
        unsafe {
            gl::Uniform1i(
                gl::GetUniformLocation(
                    self.shader_program,
                    CString::new(name).expect("CString::new failed").as_ptr(),
                ),
                value,
            );
        }
    }

    pub fn set_bool(&self, name: &str, value: bool) {
        unsafe {
            gl::Uniform1i(
                gl::GetUniformLocation(
                    self.shader_program,
                    CString::new(name).expect("CString::new failed").as_ptr(),
                ),
                value as i32,
            );
        }
    }

    pub fn set_float(&self, name: &str, value: f32) {
        unsafe {
            gl::Uniform1f(
                gl::GetUniformLocation(
                    self.shader_program,
                    CString::new(name).expect("CString::new failed").as_ptr(),
                ),
                value,
            );
        }
    }

    pub fn set_matrices(&self, model: Matrix4<f32>, view: Matrix4<f32>, projection: Matrix4<f32>) {
        unsafe {
            let model_loc = gl::GetUniformLocation(self.shader_program, c_str!("model").as_ptr());
            gl::UniformMatrix4fv(model_loc, 1, gl::FALSE, model.as_ptr());
            let view_loc = gl::GetUniformLocation(self.shader_program, c_str!("view").as_ptr());
            gl::UniformMatrix4fv(view_loc, 1, gl::FALSE, view.as_ptr());
            let projection_loc =
                gl::GetUniformLocation(self.shader_program, c_str!("projection").as_ptr());
            gl::UniformMatrix4fv(projection_loc, 1, gl::FALSE, projection.as_ptr());
        }
    }

    pub fn set_model_matrix(&self, model: Matrix4<f32>) {
        unsafe {
            let model_loc = gl::GetUniformLocation(self.shader_program, c_str!("model").as_ptr());
            gl::UniformMatrix4fv(model_loc, 1, gl::FALSE, model.as_ptr());
        }
    }

    pub fn set_view(&self, view: Matrix4<f32>) {
        unsafe {
            let view_loc = gl::GetUniformLocation(self.shader_program, c_str!("view").as_ptr());
            gl::UniformMatrix4fv(view_loc, 1, gl::FALSE, view.as_ptr());
        }
    }

    pub fn set_projection(&self, projection: Matrix4<f32>) {
        unsafe {
            let projection_loc =
                gl::GetUniformLocation(self.shader_program, c_str!("projection").as_ptr());
            gl::UniformMatrix4fv(projection_loc, 1, gl::FALSE, projection.as_ptr());
        }
    }

    pub fn set_vec3(&self, name: &str, x: f32, y: f32, z: f32) {
        unsafe {
            gl::Uniform3f(
                gl::GetUniformLocation(
                    self.shader_program,
                    CString::new(name).expect("CString::new failed").as_ptr(),
                ),
                x,
                y,
                z,
            );
        }
    }
}
