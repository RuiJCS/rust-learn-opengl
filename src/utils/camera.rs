#![allow(dead_code)]
extern crate cgmath;
use cgmath::prelude::{Angle, InnerSpace};
use cgmath::{vec3, Matrix4, Point3, Rad, Vector3};

pub struct Camera {
    pub position: Vector3<f32>,
    front: Vector3<f32>,
    up: Vector3<f32>,
    pub movement_speed: f32,
    camera_time: CameraTime,
    camera_angle: CameraAngle,
    invert_x: i8,
    invert_y: i8,
}

struct CameraTime {
    pub last_time: f32,
    pub current_time: f32,
}

struct CameraAngle {
    pub yaw: f32,
    pub pitch: f32,
    pub mouse_speed: f32,
    last_x: f64,
    last_y: f64,
    first_mouse: bool,
}

impl Camera {
    /// This method creates an Camera struct with default values (which will be added once the
    /// implementation is finished)
    pub fn new(time: f32, mouse_x: f64, mouse_y: f64) -> Self {
        let up = vec3(0.0, 1.0, 0.0);
        let position = vec3(0.0, 0.0, 3.0);
        let front = vec3(0.0, 0.0, -3.0);
        Camera {
            position: position,
            front: front,
            up: up,
            movement_speed: 5f32,
            camera_time: CameraTime::new(time),
            camera_angle: CameraAngle::new(mouse_x, mouse_y),
            invert_x: 1,
            invert_y: -1,
        }
    }

    pub fn new_with_position(
        position: Vector3<f32>,
        time: f32,
        mouse_x: f64,
        mouse_y: f64,
    ) -> Self {
        let up = vec3(0.0, 1.0, 0.0);
        let front = vec3(0.0, 0.0, -3.0);
        Camera {
            position: position,
            front: front,
            up: up,
            movement_speed: 5f32,
            camera_time: CameraTime::new(time),
            camera_angle: CameraAngle::new(mouse_x, mouse_y),
            invert_x: 1,
            invert_y: -1,
        }
    }

    /// This method calculates the direction the camera is looking
    fn direction(&self) -> Vector3<f32> {
        self.position + self.front
    }

    /// This method calculates the right vector of the camera
    fn right(&self) -> Vector3<f32> {
        self.up.cross(self.front).normalize()
    }

    /// This method calculates the up vector of the camera
    fn up(&self) -> Vector3<f32> {
        self.direction().cross(self.right()).normalize()
    }

    /// This method makes the camera move
    ///
    /// This method is to be used to move the camera forwards or backwards and to strafe left
    /// an right
    pub fn move_camera(&mut self, move_x: f32, move_y: f32, time: f32) {
        let delta = self.camera_time.delta(time);
        if move_x != 0f32 {
            self.position += self.movement_speed * self.front * move_x * delta
        }
        if move_y != 0f32 {
            self.position += self.movement_speed * self.right() * move_y * delta
        }
        self.position.y = self.position.y;
    }

    /// This method calculates the view matrix
    pub fn look_at(&mut self, time: f32) -> Matrix4<f32> {
        self.camera_time.update_time(time);
        let eye = Point3::new(self.position.x, self.position.y, self.position.z);
        let direction = self.direction();
        let center = Point3::new(direction.x, direction.y, direction.z);
        Matrix4::look_at(eye, center, self.up)
    }

    /// This method updates the direction of the camera when a mouse difference is verified
    ///
    /// This method turns the camera up or down, right or left based on the provided input
    pub fn turn_camera(&mut self, mouse_x: f64, mouse_y: f64) {
        self.camera_angle.update(
            self.invert_x as f64 * mouse_x,
            self.invert_y as f64 * mouse_y,
        );
        self.front = vec3(
            Rad::cos(Rad(self.camera_angle.pitch.to_radians() as f32))
                * Rad::cos(Rad(self.camera_angle.yaw.to_radians() as f32)),
            Rad::sin(Rad(self.camera_angle.pitch.to_radians() as f32)),
            Rad::cos(Rad(self.camera_angle.pitch.to_radians() as f32))
                * Rad::sin(Rad(self.camera_angle.yaw.to_radians() as f32)),
        )
        .normalize();
    }

    pub fn upwards(&mut self) {
        self.position.y += 0.1;
        self.front.y += 0.1;
    }

    pub fn downwards(&mut self) {
        self.position.y -= 0.1;
        self.front.y -= 0.1;
    }
}

impl CameraTime {
    /// This method creates a new CameraTime with the time passed as argument
    pub fn new(time: f32) -> Self {
        CameraTime {
            last_time: time,
            current_time: time,
        }
    }

    /// This method updates the time with more up to date values
    fn update_time(&mut self, time: f32) {
        self.last_time = self.current_time;
        self.current_time = time;
    }

    /// This method returns the diference between the last time and the current time
    pub fn delta(&mut self, time: f32) -> f32 {
        self.update_time(time);
        self.current_time - self.last_time
    }
}

impl CameraAngle {
    /// This method returns a CameraAngle with default values
    pub fn new(mouse_x: f64, mouse_y: f64) -> Self {
        CameraAngle {
            yaw: -90f32,
            pitch: 0f32,
            mouse_speed: 0.01f32,
            last_x: mouse_x,
            last_y: mouse_y,
            first_mouse: true,
        }
    }

    /// This method updates the mouse positions and the yaw and pitch values
    pub fn update(&mut self, mouse_x: f64, mouse_y: f64) {
        if self.first_mouse {
            self.last_x = mouse_x;
            self.last_y = mouse_y;
            self.first_mouse = false;
        }
        let x_offset = mouse_x - self.last_x;
        let y_offset = mouse_y - self.last_y;
        self.last_x = mouse_x;
        self.last_y = mouse_y;
        self.yaw = self.yaw + x_offset as f32 * self.mouse_speed;
        let pitch = self.pitch + y_offset as f32 * self.mouse_speed;
        self.pitch = if pitch > 89f32 {
            89f32
        } else if pitch < -89f32 {
            -89f32
        } else {
            pitch
        }
    }
}
