use image::GenericImage;
use std::{ffi::c_void, path::Path};

struct Texture {
    name: String,
    texture: u32,
}

impl Texture {
    pub fn new(texture_name: &&str) -> Self {
        let border_color: [f32; 4] = [1.0, 1.0, 0.0, 1.0];
        let image = match image::open(Path::new(*texture_name)) {
            Ok(a) => a,
            _ => unreachable!(),
        };
        let mut texture = 0;
        let data = image.raw_pixels();
        unsafe {
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_WRAP_S,
                gl::MIRRORED_REPEAT as i32,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_WRAP_T,
                gl::MIRRORED_REPEAT as i32,
            );
            gl::TexParameterfv(
                gl::TEXTURE_2D,
                gl::TEXTURE_BORDER_COLOR,
                &border_color as *const f32,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_MIN_FILTER,
                gl::LINEAR_MIPMAP_LINEAR as i32,
            );
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
            gl::GenTextures(1, &mut texture);
            gl::BindTexture(gl::TEXTURE_2D, texture);
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                gl::RGB as i32,
                image.width() as i32,
                image.height() as i32,
                0,
                gl::RGB,
                gl::UNSIGNED_BYTE,
                &data[0] as *const u8 as *const c_void,
            );
            gl::GenerateMipmap(gl::TEXTURE_2D);
        }
        Self {
            name: texture_name.to_string(),
            texture,
        }
    }

    pub fn use_texture(&self, index: u32) {
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0 + (index));
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
        }
    }
}

pub struct Textures {
    textures: Vec<Texture>,
}

impl Textures {
    pub fn new(images: Vec<&str>) -> Self {
        let textures = images.iter().map(Texture::new).collect();
        Self { textures }
    }

    pub fn use_textures(&self) {
        let mut index = 0u32;
        self.textures.iter().for_each(|s| {
            s.use_texture(index);
            index += 1;
        });
    }
}
